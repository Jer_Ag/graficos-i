

#include <iostream>
#include <SDL.h>
#include <stdio.h>

#include <string>

using namespace std;

bool Iniciar();
bool CargarMedia();
void Cerrar();

//ventana a renderizar
SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;
    
//la superficie contenida por la ventana
SDL_Surface* gScreenSurface = NULL;

//la imagen a cargar y que se mostrara en pantalla
SDL_Surface* gImage = NULL;
SDL_Surface* bImage = NULL;

SDL_Texture * texture = NULL;
SDL_Texture * backgroundtexture = NULL;

const int WIDTH = 800, HEIGHT = 600;
bool key_up=false;
bool key_down=false;
bool key_left=false;
bool key_right=false;

int x=320;
int y=240;


int main(int argc, char *argv[]){
	//Start up SDL and create window
    if( !Iniciar() )
    {
        printf( "Failed to initialize!\n" );
    }
    else
    {
        //Load media
        if( !CargarMedia() )
        {
            printf( "Failed to load media!\n" );
        }
        else
        {
			//Main loop flag
			bool quit = false;

			//Event handler
			SDL_Event Event;

			//While application is running
			while( !quit )
			{	
				//Handle events on queue
				while( SDL_PollEvent( &Event ) != 0 )
				{
					//User requests quit
					if( Event.type == SDL_QUIT )
					{
						quit = true;
					}
					if(Event.type= SDL_KEYDOWN){
						switch(Event.key.keysym.sym){
						case SDLK_LEFT:
							key_left=true;
							x-=4;
							break;
						case SDLK_RIGHT:
							key_right=true;
							x+=4;
							break;
						case SDLK_UP:
							y-=4;
							key_up=true;
							break;
						case SDLK_DOWN:
							y+=4;
							key_down=true;
							break;
						}
					}
					if(Event.type= SDL_KEYUP){
						switch(Event.key.keysym.sym){
						case SDLK_LEFT:
							key_left=false;
							break;
						case SDLK_RIGHT:
							key_right=false;
							break;
						case SDLK_UP:
							key_up=false;
							break;
						case SDLK_DOWN:
							key_down=false;
							break;
						}

					}
				}

				SDL_Rect dstrect = {x, y, 75, 100};
				SDL_RenderClear(gRenderer);
				SDL_RenderCopy(gRenderer, backgroundtexture, NULL, NULL);
				SDL_RenderCopy(gRenderer, texture, NULL, &dstrect);
				SDL_RenderPresent(gRenderer);

			}
        }
    }

    //Free resources and close SDL
    Cerrar();

    return 0;
}

bool Iniciar()
{
    //Initialization flag
    bool success = true;

    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
        success = false;
    }
    else
    {
        //Create window
        gWindow = SDL_CreateWindow( "Pirates!", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN );
		gRenderer = SDL_CreateRenderer(gWindow, -1, 0);
        if( gWindow == NULL )
        {
            printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
            success = false;
        }
        else
        {
            //Get window surface
            gScreenSurface = SDL_GetWindowSurface( gWindow );
        }
    }

    return success;
}

bool CargarMedia()
{
    //Loading success flag
    bool success = true;

    //Load splash image
    gImage = SDL_LoadBMP( "ship.bmp" );
	texture = SDL_CreateTextureFromSurface(gRenderer, gImage);
	

	bImage = SDL_LoadBMP("sea_sprite.bmp");
	backgroundtexture = SDL_CreateTextureFromSurface(gRenderer, bImage);

	SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 0);

    if( gImage == NULL )
    {
        printf( "Unable to load image %s! SDL Error: %s\n", "sea_sprite.bmp", SDL_GetError() );
        success = false;
    }
	if( bImage == NULL )
    {
        printf( "Unable to load image %s! SDL Error: %s\n", "sea_sprite.bmp", SDL_GetError() );
        success = false;
    }

    return success;
}

void Cerrar()
{
    //Deallocate surface
    /*SDL_FreeSurface( gImage );
    gImage = NULL;

	SDL_FreeSurface(bImage);
	bImage = NULL;*/
	
	//Destroy Texture
	SDL_DestroyTexture(texture);
	texture=NULL;

	SDL_DestroyTexture(backgroundtexture);
	backgroundtexture = NULL;

	//Destroy Rendererss
	SDL_DestroyRenderer(gRenderer);
	gRenderer=NULL;

    //Destroy window
    SDL_DestroyWindow( gWindow );
    gWindow = NULL;
	SDL_FreeSurface(gImage);
	SDL_FreeSurface(bImage);
    //Quit SDL subsystems
    SDL_Quit();
}
